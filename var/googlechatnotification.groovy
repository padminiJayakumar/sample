#!/usr/bin/env groovy

def call (status,usr,roomid) {
    if (status == 'STARTED') {
        googlechatnotification url: "id:${roomid}",message: "***STARTED****", notifySuccess: 'false';
    }
}
