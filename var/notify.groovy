#!/usr/bin/env groovy


def call(buildStatus) {

  
  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
 
  def details = ''' 
  '${BUILD_STATUS}' 
  JOB_NAME-'${JOB_NAME} 
  BUILD_NUMBER -'${BUILD_NUMBER}'
  BUILD_URL -'${BUILD_URL}' 
    '''


  googlechatnotification url: 'https://chat.googleapis.com/v1/spaces/AAAAClzUo3U/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=yH93yoaHi_rENlymY9BmhUrM6HZBeJe8vTEpZOLo9zo%3D', message:''' 
'$BUILD_STATUS'
JOB_NAME : '$JOB_NAME'
JOB_URL : '$JOB_URL'
BUILD NUMBER : '$BUILD_NUMBER' 
''', notifyAborted: 'true', notifyFailure: 'true', notifyNotBuilt: 'true', notifySuccess: 'true', notifyUnstable: 'true', notifyBackToNormal: 'true', suppressInfoLoggers: 'true', sameThreadNotification: 'true'


  emailext (
      to: 'padmini.j@vvdntech.in',
      subject: subject,
      body: details,    
    )




}
